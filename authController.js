const User = require('./models/User')
const Role = require('./models/Role')
const bcrypt = require('bcryptjs')
const {validationResult} = require('express-validator')
const jwt = require('jsonwebtoken')
const {secret} = require('./config')
const User_Role = require('./models/User_Role')


const generateAccessToken = (id, role_id) => {
    const payload = {
        id,
        role_id

    }
    return jwt.sign(payload, secret, {expiresIn: "24h"})
}

class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(400).json({message: "Empty", errors})
            }
            const {first_name, last_name, email, password} = req.body
            const candidate = await User.findOne({email})
            if (candidate) {
                return res.status(400).json({message: "Пользователь с таким Эмаилом уже есть"})
            }
            const hashPassword = bcrypt.hashSync(password, 7)
            // const role_id = await User_Role.findOne({value: ["role_id"]})
            const roleId = await Role.findOne({value: 'USER'})
            // const user = new User({first_name, last_name, email, password: hashPassword, role_id })
            // await user.save()
            const userData = {
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: hashPassword
            }
            const user = await User.create({...userData})
            const userRole = await User_Role.create({user_id: user._id, role_id: roleId._id})
            return res.json({message: "пользователь успешно зарегестрирован"})
        } catch (e) {
            console.log(e)
            res.status(400).json({message: 'registration error'})

        }

    }

    async login(req, res) {
        try {
            const {first_name, last_name, email, password} = req.body
            const user = await User.findOne({email})
            const userRole = await User_Role.findOne({user_id: user._id})
            const role_id = userRole.role_id
            if (!user) {
                return res.status(400).json({message: `${email} does not found`})
            }
            const validPassword = bcrypt.compareSync(password, user.password)
            if (!validPassword) {
                return res.status(400).json({message: `${email} parol is not true`})

            }
            const token = generateAccessToken(user._id, role_id)
            return res.json({token})

        } catch (e) {
            console.log(e)
            res.status(400).json({message: 'login error'})

        }

    }

    async getUsers(req, res) {
        try {
            // "Создал админа,юзера,модератора"
            // const userRole= new User_Role({value:"USER"})
            // const adminRole=new User_Role({value:"ADMIN"})
            // const moderatorRole= new User_Role({value:"MODERATOR"})
            // await userRole.save()
            // await adminRole.save()
            // await moderatorRole.save()
            // res.json("server work")
            const users = await User.find()
            res.json(users)
        } catch (e) {
            console.log(e)
            res.status(400).json({message: 'error'})

        }


    }

    async update(req, res) {
        try {
            const {id} = req.params
            const data = req.body
            const updatedUser = await User.findByIdAndUpdate(id, data)
            return res.json(updatedUser)

        } catch (e) {
            res.status(500).json(e.message)
        }
    }

    async delete(req, res) {
        try {
            const deleted = await User.findByIdAndDelete(req.params.id)
            return res.json(deleted)

        } catch (e) {
            res.status(500).json(e.message)

        }

    }
}

module.exports = new authController()