const {Schema,model}= require('mongoose')


const userRole = new Schema({
    user_id: {type: Schema.Types.ObjectId, ref: 'User'},
    role_id: {type: Schema.Types.ObjectId, ref: 'Role'}

})


module.exports = model('userRole',userRole)