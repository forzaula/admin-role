const jwt = require('jsonwebtoken')
const {secret} = require('../config')
const Role = require('../models/Role')


module.exports = function (roles) {
    return async function (req, res, next) {
        if (req.method === "OPTIONS") {
            next()
        }
        try {
            const token = req.headers.authorization.split(' ')[1]
            if (!token) {
                return res.status(403).json({message: "пользователь не авторизован"})
            }
            const  userRoles = jwt.verify(token, secret)
            const {role_id} = userRoles
            const findRole = await Role.findById(role_id)
            findRole.value !== 'ADMIN' && findRole.value !== 'MODERATOR' ? res.json({message: 'Fuck off'}) : next()

        } catch (e) {
            console.log(e)
            return res.status(403).json({message: e.message})
        }


    }
}