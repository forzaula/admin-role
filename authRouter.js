const Router = require('express')
const router = new Router()
const controller = require('./authController')
const {check} = require('express-validator')
const authMiddleware = require('./middlewaree/authMiddleware')
const roleMiddleware = require('./middlewaree/roleMiddleware')


router.post('/registration', [check('email', "no empty ").notEmpty(),
    check('password', "more than 4").isLength({min: 4, max: 10})], controller.registration)
router.post('/login', controller.login)
router.get('/users', authMiddleware, controller.getUsers)
router.put('/users/:id', roleMiddleware(['ADMIN', 'MODERATOR']),controller.update)
router.delete('/users/:id', roleMiddleware(['ADMIN']), controller.delete)


module.exports = router